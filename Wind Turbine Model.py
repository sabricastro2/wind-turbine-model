import tkinter as tk
from tkinter import ttk
import math

#class for storing and displaying welcome page and calculator frames
class turbineApp(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        self.geometry("630x350")
        self.title("Wind Turbine Calculator for Turbines Up to 1MW")
        container= tk.Frame(self)
        container.pack(side= "top", fill= "both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)
        self.frames= {}
        for F in (welcomePage, Calculator):
            frame= F(container, self)
            self.frames[F]= frame
            frame.grid(row=0, column=0, sticky= "nsew")
        self.show_frame(welcomePage)
    def show_frame(self, cont):
        frame= self.frames[cont]
        frame.tkraise()

class welcomePage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent, bg="white")
        labelTitle= tk.Label(self, text="Hello!", font="LARGEFONT", bg="white")
        labelTitle.grid(row=0, padx=10, pady=10)
        labelTitle.grid_rowconfigure(1, weight= 1)
        labelTitle.grid_columnconfigure(1, weight=1)
        description= """I created this calculator for users like you to be able to calculate basic wind turbine 
        characteristics including Reynolds Number for the wind flow on the blades, the lift and drag forces on one 
        blade, the torque of the blade, and the power of the turbine using the turbine's tip speed ratio (TSR). These 
        outputs can be calculated by providing air properties and particular wind turbine dimensions. For air 
        properties, you will need to provide the air density, average air temperature, and average wind speed of the 
        chosen site of the project. For wind turbine dimensions, you will need to provide the following: number of 
        blades, length of the blade, width of the blade, hub radius, pitch angle of the blade, the wind turbine's 
        rated power in kilowatts, and the TSR. This calculator uses METRIC units."""

        labelDescription= tk.Label(self, text= description, bg="white")
        labelDescription.grid(row=1)
        labelDescription.grid_rowconfigure(0, weight=1)
        labelDescription.grid_columnconfigure(0, weight=1)
        labelHowTo = tk.Label(self, text="How to Use:", font="LARGEFONT", bg="white")
        labelHowTo.grid(row=2, padx=10, pady=10)
        labelHowTo.grid_rowconfigure(1, weight=1)
        labelHowTo.grid_columnconfigure(1, weight=1)
        howToDesc= """Type the value next to each corresponding section and hit the "Calculate" button when each entry 
        box is filled to run the calculator. The outputs will display to the right next to each output label."""
        labelHowToDesc = tk.Label(self, text=howToDesc, bg="white")
        labelHowToDesc.grid(row=3)
        labelHowToDesc.grid_rowconfigure(0, weight=1)
        labelHowToDesc.grid_columnconfigure(0, weight=1)

        ttk.Button(self, text="Go to Calculator",
                             command=lambda: controller.show_frame(Calculator)).grid(row=4, column=0, padx=50, pady=50)

class Calculator(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        # Background
        # pic = tk.PhotoImage(file="SG Image.png")
        self.img = tk.PhotoImage(file="SG Image.png")
        img_label= tk.Label(self, image=self.img)
        img_label.place(x=0, y=0)
        img_text= tk.Label(text="Calculating Reynolds Number, Lift/Drag Forces, Torque, and Power Output for Wind Turbine Blades (to 1 MW)")
        # Air Properties at Site
        tk.Label(self, text= "Air Properties at Site", font="bold", bg="white").grid(row=0)
        tk.Label(self, text= "Air Density [kg/m³]", bg="white").grid(sticky="e", row=1)
        tk.Label(self, text= "Air Temperature [\N{DEGREE SIGN}C]", bg="white").grid(sticky="e", row=2)
        tk.Label(self, text= "Air Velocity [m/s]", bg="white").grid(sticky="e", row=3)
        e1= tk.Entry(self)
        e2= tk.Entry(self)
        e3= tk.Entry(self)
        e1.grid(row=1, column=1)
        e2.grid(row=2, column=1)
        e3.grid(row=3, column=1)

        # Wind Turbine Dimensions
        tk.Label(self, text= "Wind Turbine Dimensions", font= "bold", bg="white").grid(sticky="e", row=4)
        tk.Label(self, text= "Number of Blades", bg="white").grid(sticky="e", row=5)
        tk.Label(self, text= "Length of Blade [m]", bg="white").grid(sticky="e", row=6)
        tk.Label(self, text= "Width of Blade [m]", bg="white").grid(sticky="e", row=7)
        tk.Label(self, text= "Hub Radius [m]", bg="white").grid(sticky="e", row=8)
        tk.Label(self, text= "Pitch Angle [\N{DEGREE SIGN}]", bg="white").grid(sticky="e", row=9)
        tk.Label(self, text= "Rated Power [kW]", bg="white").grid(sticky="e", row=10)
        tk.Label(self, text= "Tip Speed Ratio", bg="white").grid(sticky="e", row=11)
        e5= tk.Entry(self)
        e6= tk.Entry(self)
        e7= tk.Entry(self)
        e8= tk.Entry(self)
        e9= tk.Entry(self)
        e10= tk.Entry(self)
        e11= tk.Entry(self)
        e5.grid(row=5, column=1)
        e6.grid(row=6, column=1)
        e7.grid(row=7, column=1)
        e8.grid(row=8, column=1)
        e9.grid(row=9, column=1)
        e10.grid(row=10, column=1)
        e11.grid(row=11, column=1)

        # Outputs
        tk.Label(self, text= "Reynolds Number: ", bg="white").grid(sticky="e", row=1, column= 2, padx=(50,0))
        tk.Label(self, text= "Lift Force on Blades [N]: ", bg="white").grid(sticky="e", row=3, column= 2, padx=(50,0))
        tk.Label(self, text= "Drag Force on Blades [N]: ", bg="white").grid(sticky="e", row=5, column= 2, padx=(50,0))
        tk.Label(self, text= "Torque of Blades [Nm]: ", bg="white").grid(sticky="e", row=7, column= 2, padx=(50,0))
        tk.Label(self, text= "Power Output of Blades [kW]: ", bg="white").grid(sticky="e", row=9, column= 2, padx=(50,0))

        def calculate_outputs():
            dens= float(e1.get())  # Air density [kg/m^3]
            temp= float(e2.get())  # Air temperature [degree C]
            vel= float(e3.get())  # Air flow velocity [m/s]
            numBlades= float(e5.get())  # Number of Blades
            lenBlade= float(e6.get())  # Length of Blades [m]
            widthBlade= float(e7.get())  # Width of Blades [m]
            hubR= float(e8.get())  # Hub Radius [m]
            pitch= float(e9.get())  # Pitch Angle [degrees]
            pwr= float(e10.get())  # Turbine Rated Power [kW]
            tsr= float(e11.get())  # Tip Speed Ratio
            A=lenBlade*widthBlade  # Blade Area [m^2]
            # Dynamic Viscosity: D.M. Sutherland Eqn (Crane, 1988) mu = mu0 * ((T / T0)^1.5) * (a/b)
                # mu is in centipoise and T is in Rankine
                # a= 0.555T0+C, b= 0.555T+C
                    # C= 120 (Sutherland's Constant for Standard Air)
                # Convert T(R)= (T(C)+273.15)*(9/5), mu0= 0.01827 centipoise, T0= 524.07 R
            mu0= 0.01827 # kg/m*s to lbf*s/ft^2 (air dynamic viscosity at sea level)
            T0= 524.07 # deg. C to deg. Rankine(air temp at sea level)
            tempAdj= (temp+273)*(9/5) # Convert user input temp from Celcius to Rankine
            mu= (mu0*((tempAdj/T0)**1.5)*((0.555*T0+120)/(0.555*tempAdj+120)))*.001 #Adjusted dynamic viscosity (converted back to kg.m*s)
            # Reynolds Number
            Re= "{:e}".format((dens*vel*widthBlade)/mu)
            tk.Label(self, text=Re, bg="white").grid(sticky= 'w', padx= (25,0), row=1, column=3)
            # Lift and Drag Forces
            # Get drag coefficient
            if pwr<20: # Airfoil S803
                cL=1.5
                cD= 0.006
            elif pwr>= 20 and pwr<100: # Airfoil S820
                cL=1.1
                cD= 0.007
            elif pwr>=100 and pwr<400: # Airfoil S813
                cL= 1.1
                cD= 0.007
            elif pwr>=400 and pwr<=1000: # Airfoil S817
                cL= 1.1
                cD= 0.007
            drag= round(0.5*cD*dens*(vel**2)*A, 3)
            lift= round(0.5*cL*dens*(vel**2)*A, 3)
            tk.Label(self, text=lift, bg="white").grid(sticky= 'w', padx= (25,0), row=3, column=3)
            tk.Label(self, text=drag, bg="white").grid(sticky= 'w', padx= (25,0), row=5, column=3)
            # Torque
            Q= round((lift*math.sin(math.radians(pitch))-drag*math.cos(math.radians(pitch)))*lenBlade,3)
            tk.Label(self, text=Q, bg="white").grid(sticky= 'w', padx= (25,0), row=7, column=3)
            # Power
            Rtot= hubR+lenBlade
            angVel= tsr*vel/Rtot
            P= round((numBlades*Q*angVel)*.001, 3) # Power in kW
            tk.Label(self, text= P, bg="white").grid(sticky= 'w', padx= (25,0), row=9, column= 3)
        ttk.Button(self, text= "Calculate",width= 20, command= calculate_outputs).grid(row=14, pady=(50,50))
        ttk.Button(self, text="Back to Home",
                             command=lambda: controller.show_frame(welcomePage)).grid(row= 14, column= 3, pady= (50,50))

app = turbineApp()
app.mainloop()

